#requires -version 2
<#
.SYNOPSIS
  <>
.DESCRIPTION
  <Powershell script can help you quickly find and extract files from ue-v profile>
.PARAMETER <Parameter_Name>
    <
    profilePath - path to profile, 
    extractPath - where you want extract files
    >
.INPUTS
  <Inputs if any, otherwise state None>
.OUTPUTS
  <Outputs if any, otherwise state None - example: Log file stored in C:\Windows\Temp\<name>.log>
.NOTES
  Version:        1.0
  Author:         <Name>
  Creation Date:  <Date>
  Purpose/Change: Initial script development
  
.EXAMPLE
  <extract-uevfiles -profilePath <profile> -extractPath <somefolder>
#>

Function extract-uevfiles {

    Param (
        $profilePath,
        $extractPath
    )

    $profileApplications = Get-Item ($profilePath + "\SettingsPackages\*")
    $profileFolderName = (Get-Item $profilePath).Name

    if (!(Test-Path ($extractPath + "\" + ($profilePath).Name)))
    {
        New-Item -Path $extractPath -Name $profileFolderName -ItemType Directory | Out-Null
        }
    
    $profileApplication = $profileApplications | select Name, fullname| `
                            Out-GridView -Title "Select application" -OutputMode Single

    $applicationPKGX = Get-Item ($profileApplication.fullname + "\*") | `
                        Where-Object {$_.Name -like "*.pkgx"} | `
                            Select-Object -First 1

    $applicationPKGDAT = (Get-Item ($profileApplication.fullname + "\*") | `
                            Where-Object {$_.Name -like "*.PKGDAT"}).Name

    $XMLcontent = (Export-UevPackage $applicationPKGX.fullname)

    $Strings = ($XMLcontent).split(“`n”) | `
                    select-string VT_FILE

    $objects = @()
    foreach ($String in $Strings)
    {


        $hash1 = $String -split ‘<Setting Type=|Name=|Action=|</Setting>’
        $folder = $hash1[2].split(“\”)[1]
        $filename = $hash1[2].split(“\”)[-1].Replace(‘"‘,“”)
        $pkgdat= $hash1[3].Split(“>”)[1]

        if ($pkgdat)
            {
                $object = New-Object psobject -Property @{
                                                        folder = $folder
                                                        filename = $filename
                                                        pkgdat = $pkgdat
                                                    }
    
        
        if ($applicationPKGDAT -contains $object.pkgdat)
            {
              $objects += $object
                }
            }
    }

    $selectedFiles = $objects | Out-GridView -Title "Select Suffixus and Click OK" -OutputMode Multiple
    $totalPath = ($extractPath + "\" + $profileFolderName)

    if ($selectedFiles)
    {
        foreach ($item in $selectedFiles)
        {
            $targetFolder = ($totalPath  + "\" + $item.folder)

            if (Test-Path $targetFolder)
            {
                Copy-Item ($profileApplication.FullName + "\" + $item.pkgdat) ($targetFolder + "\" + $item.filename) -Verbose
                }
                else 
                {
                    New-Item -Path $totalPath -Name $item.folder -ItemType Directory | Out-Null
                    Copy-Item ($profileApplication.FullName + "\" + $item.pkgdat) ($targetFolder + "\" + $item.filename) -Verbose
                    }

        }
    }

}
